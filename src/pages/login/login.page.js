import React from 'react';
import { withFirebase } from 'react-redux-firebase';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

export class LoginPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: ''
        }

        //
        this.loginWithEmailPassword = this.loginWithEmailPassword.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    loginWithEmailPassword(e) {
        e.preventDefault();
        const { login } = this.props.firebase;

        login({
            email: this.state.email,
            password: this.state.password
        }).then(
            (fulfilled) => this.handleLoginFulfilled(fulfilled),
            (rejected) => this.handleLoginRejected(rejected)
        );
    }

    handleLoginFulfilled(e) {
        console.log('LOGIN SUCCESS', e);
    }

    handleLoginRejected(e) {
        console.warn('LOGIN FAILED', e);
        switch (e.code) {
            case 'auth/wrong-password':
                break;
            case 'auth/invalid-email':
                break;
            case 'auth/user-not-found':
                break;
            default:
                break;
        }
    }

    handleInputChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    render() {
        return (
            <div>
                <p>Placeholder login</p>
                <Card>
                    <CardContent>
                        <form onSubmit={this.loginWithEmailPassword}>
                            Email
                    <TextField type="text" name="email" value={this.state.email} onChange={this.handleInputChange} required />
                            Password
                    <TextField type="password" name="password" value={this.state.password} onChange={this.handleInputChange} required />
                            <Button variant="contained" component="span" type="submit">Submit</Button>
                        </form>
                    </CardContent>
                </Card>

            </div >
        );
    }
}

export default withFirebase(LoginPage);
