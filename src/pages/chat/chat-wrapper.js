import React from 'react';
import ChatDrawer from './chat-drawer';
import ChatPage from './chat.page';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import NoChatSelectedPage from './no-chat-selected-page';

import './chat-wrapper.css';

const ChatWrapper = () => {
    return (
        <div>
            <div className="ChatDrawer">
                <ChatDrawer />
            </div>
            <div className="ChatPage">
                <Route path="/chat/:chatId" component={ChatPage} />
                <Route exact path="/chat" component={NoChatSelectedPage} />
            </div>
        </div>
    )
}

export default ChatWrapper;
