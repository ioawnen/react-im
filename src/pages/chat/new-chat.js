import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import { TextField } from '@material-ui/core';

const NewChat = ({onClose}) => {

    return (
        <Dialog onClose={onClose}>
            <DialogTitle>
                Create New
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Blah blah
                    </DialogContentText>
                    <TextField autoFocus margin="dense" id="new_chat_name" label="Chat Name" type="text" fullWidth />
                </DialogContent>
                <DialogActions>
                    
                </DialogActions>
        </Dialog>
    )
};

NewChat.defaultProps = {
    onClose: () => console.warn('onClose not handled. Doing nothing.')
};

export default NewChat;