import './chat-bubble.css';
import React from 'react';
import * as moment from 'moment';

export const ChatBubble = ({ message, currentUser }) => {
    let bubbleClass = 'Bubble-Wrapper';
    bubbleClass += currentUser.userId === message.from ? ' Current-User' : ' Other-User'
    return (
        <div className={bubbleClass} >
            <div className="Bubble">
                <span className="Message-Area">
                    {message.message}
                </span>
            </div>
            <div className="Details-Area">
                <span className="Details-Area--From">{message.from}</span>
                <span className="Details-Area--Time"
                    title={`Sent ${moment(message.time).format("dddd, MMMM Do YYYY, h:mm:ss a")}`}>
                    {moment(message.time).calendar()}
                </span>
            </div>
        </div >
    )
}

export default ChatBubble;
