import React from 'react';
import { compose } from 'redux'
import { connect } from 'react-redux'
import {
    firebaseConnect,
    getVal,
    isLoaded,
    isEmpty
} from 'react-redux-firebase';
import { ChatBubble } from './chat-bubble';
import ChatInputBox from './chat-input-box';
import * as moment from 'moment';
import './chat.page.css';
import SimpleBar from 'simplebar-react';

import 'simplebar/dist/simplebar.min.css';




export class ChatPage extends React.Component {
    /**
     * Ref to hidden element that sits at the bottom of the chat bubbles. Used as a scroll target.
     *
     * @memberof ChatPage
     */
    bubblesEndEl;

    constructor(props) {
        super(props);
        this.state = {
            currentUser: { userId: 'exampleID' },
            atBottom: false
        };

        // METHOD BINDINGS
        this.onSubmit = this.onSubmit.bind(this);
        // END
    }

    /**
     * Scrolls to the bottom of the chat bubbles container.
     *
     * @memberof ChatPage
     */
    scrollToBottom = ({ smooth } = { smooth: false }) => {
        if (this.bubblesEndEl) {
            this.bubblesEndEl.scrollIntoView({ behavior: smooth ? 'smooth' : 'auto' });
        }
    }
    
    componentDidUpdate() {
        if (this.state.atBottom) {
            this.scrollToBottom({ smooth: true });
        }
    }

    /**
     * @description Submits an input message string as the current user
     * @author ioawnen
     * @date 2019-03-04
     * @param {*} e Message string
     * @memberof ChatPage
     */
    onSubmit(e) {
        this.props.firebase.push(
            `chats/${this.props.match.params.chatId}/messages`,
            {
                time: moment().valueOf(),
                from: 'exampleID',
                message: e
            }
        );
    }

    /**
     * Handles bubblesContainer event bindings when first mounting.
     *
     * @memberof ChatPage
     */
    bubblesContainerDidMount = (ref) => {
        this.scrollToBottom(); // Scroll to bottom first time
        ref.addEventListener('scroll', () => {
            const bottomZone = 20; // Bottom position grace zone. Add more to auto scroll from further away
            const atBottom = ref.scrollTop >= ref.scrollHeight - ref.getBoundingClientRect().bottom - bottomZone;
            if (atBottom !== this.state.atBottom) { // Only update if we have to
                this.setState({ atBottom: atBottom });
            }
        });
    }


    render() {
        const { chat } = this.props;

        if (!isLoaded(chat)) {
            return 'Loading....';
        }
        if (isEmpty(chat)) {
            return 'Chat data not found. Is the ID correct?';
        }
        return (
            <div className="ChatPageWrapper">
                <div className="ChatBubblesContainer" ref={this.bubblesContainerDidMount}>
                    {
                        isLoaded(chat) && !isEmpty(chat.messages) && Object.values(chat.messages).map(
                            (message, index) =>
                                <ChatBubble key={index} message={message} currentUser={this.state.currentUser} />
                        )
                    }
                    <div name="_bubblesEndScrollTarget" ref={(r) => { this.bubblesEndEl = r; }} />
                </div>
                <div className="ChatInputBoxContainer">
                    <ChatInputBox onChange={this.onInputValueChange} onSubmit={this.onSubmit} />
                </div>
            </div>
        );
    }
}

export default compose(
    firebaseConnect((props) => [
        `chats/${props.match.params.chatId}`
    ]),
    connect(({ firebase }, props) => ({
        chat: getVal(firebase, `data/chats/${props.match.params.chatId}`)
    }))
)(ChatPage);
