import React from 'react';
import './no-chat-selected-page.css';

const NoChatSelectedPage = () => {

  return (
    <div className="NoChatPageWrapper">
      <div className="NoChatPageCenterContainer">
        Select a chat from the left panel
    </div>
    </div >
  )
};

export default NoChatSelectedPage;
