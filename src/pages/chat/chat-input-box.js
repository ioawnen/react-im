import React from 'react';
import { FormControl, InputLabel, InputAdornment, IconButton, Input, TextField } from '@material-ui/core';

import './chat-input-box.css';

import ArrowForward from '@material-ui/icons/ArrowForward';

export class ChatInputBox extends React.Component {

    static defaultProps = {
        onChange: (e) => { },
        onSubmit: (e) => { }
    }

    constructor(props) {
        super(props);

        this.state = {
            value: ''
        };

        this.inputChange = this.inputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    inputChange(event) {
        const { value } = event.target;
        this.setState({ value: value });
        this.props.onChange(value);
    }

    onSubmit(event) {
        event.preventDefault();
        this.props.onSubmit(this.state.value);
        this.setState({ value: '' });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    <FormControl fullWidth>
                        <TextField
                            id="outlined-multiline-flexible"
                            label=""
                            rowsMax="4"
                            value={this.state.value}
                            onChange={this.inputChange}
                            margin="normal"
                            InputProps={{
                                autoComplete: "off",
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="Toggle password visibility"
                                            onClick={this.onSubmit}>
                                            <ArrowForward />
                                        </IconButton>
                                    </InputAdornment>
                                )
                            }}
                        />
                    </FormControl>
                </form>
            </div>
        )
    }
}

export default ChatInputBox;
