import React from 'react';
import { compose } from 'redux'
import { connect } from 'react-redux'
import {
    firebaseConnect,
    getVal,
    isLoaded,
    isEmpty
} from 'react-redux-firebase';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Add from '@material-ui/icons/Add';
import Chat from '@material-ui/icons/Chat';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

import { Link } from "react-router-dom";

import './chat-drawer.css';

const ChatDrawer = ({ chats, onNewChatClick, expandedChange }) => {

    return (
        <Drawer variant="persistent"
            anchor="left"
            open={true}>
            <div>
                <List>
                    <ListItem button key="newChatButton" onClick={onNewChatClick()}>
                        <ListItemIcon><Add /></ListItemIcon>
                        <ListItemText primary="New" />
                    </ListItem>
                </List>
                <Divider />
                <List>
                    {
                        isLoaded(chats) && Object.keys(chats).map((key) => {
                            const { name } = chats[key];
                            return (
                                <Link to={`/chat/${key}`} key={`chatButton_${key}`}>
                                    <ListItem button>
                                        <ListItemIcon><Chat /></ListItemIcon>
                                        <ListItemText primary={name} />
                                    </ListItem>
                                </Link>
                            )
                        })
                    }
                </List>
                <Divider />
                <List>
                    <ListItem button key="collapseButton" onClick={expandedChange()}>
                        <ListItemIcon><KeyboardArrowLeft /></ListItemIcon>
                        <ListItemText primary="Collapse" />
                    </ListItem>
                </List>
            </div>
        </Drawer>
    );
}

ChatDrawer.defaultProps = {
    onNewChatClick: () => { },
    expandedChange: (expanded) => { }
}

export default compose(
    firebaseConnect((props) => [
        'chats'
    ]),
    connect(({ firebase }, props) => ({
        chats: getVal(firebase, `data/chats`)
    }))
)(ChatDrawer);