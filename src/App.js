import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './Home'
import configureStore from './store'
import './App.css';

import LoginPage from './pages/login/login.page';
import ChatWrapper from './pages/chat/chat-wrapper';

// eslint-disable-next-line no-mixed-operators
const initialState = window && window.__INITIAL_STATE__ || { firebase: { authError: null } }; // set initial state here
const store = configureStore(initialState);

export default () => (
  <Provider store={store}>
    <Router>
      <div>
        <Route path="/login" component={LoginPage} />
        <Route path="/chat" component={ChatWrapper} />
        <Route exact path="/" component={Home} />
      </div>
    </Router>
  </Provider>
)
