import React from 'react'
import './App.css'
import { withFirebase } from 'react-redux-firebase';

const Home = ({ firebase }) => {
  console.log('HOME', firebase);
  firebase.push('testData', { 'hello': 'world' })
  return (
    <div className='App'>
      DUMMY HOME
    </div >
  )
}

export default withFirebase(Home)
